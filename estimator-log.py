#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from steem import Steem
from steem.post import Post
from steem.amount import Amount
from steem.account import Account
from dateutil.parser import parse
from datetime import datetime 
from datetime import timedelta
import os
# import logging
from sqlitelogger import SQLiteLogger
import sqlite3
from sys import argv


from steem.steemd import Steemd
from steem.instance import set_shared_steemd_instance
# https://stackoverflow.com/questions/5973120/what-is-the-replacement-for-dateutil-parser-in-python3


### Curation reward penalty
def curation_penalty(post, vote):
    post_time = post["created"]
    vote_time = parse(vote["time"])
    time_elapsed = vote_time - post_time
    reward = time_elapsed / timedelta(minutes=30) * 1.0

    if reward > 1.0:
        reward = 1.0
    return reward

### Calculating curation reward per vote
curation_pct = 0.25
def curation_reward(post, vote, rshare, base):
    rshares = float(vote["rshares"])
    base_share = rshare * base
    return (rshares * curation_penalty(post, vote) * curation_pct) * base_share

### Calculating beneficiary shares
def beneficiaries_pct(post):
    weight = sum([beneficiary["weight"] for beneficiary in post["beneficiaries"]])
    return  weight / 10000.0

def estimate_votes(post):
    return [vote for vote in post["active_votes"]]

def estimate_total_share(votes, post):
    return sum([float(vote["rshares"]) * reward_share * base for vote in votes])

def estimate_curation_share(reward_share,base,votes, post):
    curation_share = sum([curation_reward(post, vote, reward_share, base) for vote in votes])
    return curation_share

def estimate_author_share(total_share,curation_share, post):
    return (total_share - curation_share) * (1.0 - beneficiaries_pct(post))

def estimate_beneficiary_share(total_share,curation_share, post):
    return (total_share - curation_share) * beneficiaries_pct(post)


steemd_nodes = [
    'https://api.steemit.com',
    'https://steemd.minnowsupportproject.org',
    'https://steemd.privex.io',

]
set_shared_steemd_instance(Steemd(nodes=steemd_nodes))

STEEM_KEY = os.getenv('STEEM_KEY') # You can use a env variable for the WIF here.
steem = Steem(keys = [STEEM_KEY])

# My SQL logger module thing.
logger = SQLiteLogger()
database = "steemit-transactions.sqlite"

conn = logger.connection(database)
logger.setup_tables(conn)


### Estimating rewards in last `N` days
account = argv[1]
#account = "berniesanders"
time_period  = timedelta(days=3)

reward_fund = steem.get_reward_fund()
reward_balance = Amount(reward_fund["reward_balance"]).amount
recent_claims = float(reward_fund["recent_claims"])
reward_share = reward_balance / recent_claims
base = Amount(steem.get_current_median_history_price()["base"]).amount

# # print("Reward Fund:", reward_fund)
# print("Reward Balance:", reward_balance)
# print("Reward Claims:", recent_claims)
# print("Reward Share:", reward_share)
# print("Reward Base:", base)


posts = set()
for post in Account(account).history_reverse(filter_by="comment"):
    try:
        post = Post(post)
        if post.time_elapsed() < time_period:
            if post.is_main_post() and not post["title"] in posts:
                posts.add(post["title"])
                post_title = post["title"]
                post_url = "https://steemit.com" + post["url"]
                post_created_date = post["created"]
                print("Post created date: ", post_created_date)
                print("Post url is: ", post_url)
                votes = estimate_votes(post)
                print("Printing out the Total Share below:")
                total_share = estimate_total_share(votes,post)
                print(total_share)
                print("Printing out curation share: ")
                curation_share = estimate_curation_share(reward_share,base,votes,post)
                print(curation_share)
                author_share = estimate_author_share(total_share,curation_share,post)
                beneficiary_share = estimate_beneficiary_share(total_share,curation_share,post)
                print("Estimated total reward for this post is $", total_share)
                logger.load_earninglog_table(post_url,post_created_date,post_title,account, total_share,curation_share, conn)

                

        else:
            break
    except Exception as error:
        continue

logger.close_conection(conn)