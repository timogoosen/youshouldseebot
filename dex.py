#!/usr/bin/env python3
#-*- coding: utf-8 -*-
import os
from steem import Steem

from steem.dex import Dex

BOT_NAME = "throwawayaccount"

# @bretton
# at most basic level, list buys and sells concurrently, with a spread. keep doing it every hour | few hours | day, and over time both will close and you'll see one side keep growing
# more complicated things might be deriving bollinger bands, and when price goes outside them, do something
# and even more complicated, compare 30min and 4h bbands, look for cross-over gaps and things for triggers. but this is several days of explaining :joy:
# simple is best. buy & sell with a spread, small amounts, so you can do it a long time.
# over time, both positions close, and you end up with more of one, ideally btc

# Other useful hints
# - check balances before placing trade
# - create a checklist of variables, and only when all are in the clear, make the trade(s)
# examples may include:
# - is calculated buy price below sell price
# - am I within the range of prices I've set as limits
# - is my buy balance sufficient, is my sell balance sufficient

STEEM_KEY = os.getenv('STEEM_KEY') # You can use a
s = Steem(keys = [STEEM_KEY])


# buy_sell_dex_exchange_history = dex_t.market_history(bucket_seconds=300,start_age=3600,end_age=0)
# print(buy_sell_dex_exchange_history)


# Get balance, put this in function.



sbd_balance = s.get_account(BOT_NAME)['sbd_balance']
steem_balance = s.get_account(BOT_NAME)['balance']


dex_t = Dex(steemd_instance=s)

print("SBD Balance: ", sbd_balance)
print("Steem Balance: ", steem_balance)

# Check steem ask/sell price

# Check steem want/buy price


# Create bid of %10 greater than highest bid.


# Check for function that checks for bids

dex_ticker = dex_t.get_ticker()


lowest_sell = (float(dex_ticker['lowest_ask'])) 

highest_buy = (float(dex_ticker['highest_bid'])) 


print("Lowest Sell: ", lowest_sell)
print("Highest buy: ", highest_buy)