## Install Dependencies:

Install dependencies with:

```

$ pipenv install steem


```


## Usage:

Export WIF key with env variable:

```
$ STEEM_KEY="WIF KEY HERE"


$ pipenv run python3 upvote.py

```

Remember the key that you should use is the active key, the active key is used for: "The active key is used to make transfers and place orders in the internal market."

### Some Required Reading:

Dex Stuff:

https://steemit.com/steemit/@rvanstel/two-weeks-of-trading-on-the-steemit-internal-market-let-s-see-how-i-ve-done

https://steemit.com/steemit/@rvanstel/why-you-should-trade-on-the-steemit-internal-market

https://steemit.com/linux/@throwawayaccount/access-the-steem-dex-with-python

### Use Multiple API Nodes:

Check here: 
https://github.com/Netherdrake/steem-python/blob/master/docs/steem.rst

see the "Setting Custom Nodes" section.



### TODO:

* Add sqlite logging table called: "Accounting"

* Add telegram notifications for any type of trade happening.

