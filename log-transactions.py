import json
import datetime
import os
from steem import Steem
from steem.account import Account
from steem.post import Post
#import random
import time
# My own code:
from sqlitelogger import SQLiteLogger
import sqlite3


ACCOUNT_TO_MONITOR = "smartsteem"

STEEM_KEY = os.getenv('STEEM_KEY') # You can use a
s = Steem(keys = [STEEM_KEY])

acc = Account(ACCOUNT_TO_MONITOR, steemd_instance=s)


# def convert_to_currency(transaction_amount):
logger = SQLiteLogger()
database = "steemit-transactions.sqlite"

conn = logger.connection(database)
logger.setup_tables(conn)


while True:
    transfers = acc.get_account_history(-1, 250, filter_by=["transfer"])
    # This will not return latest 100 transfers. Because filtering is done on the steem-python (client) side, so it will get last 100 operations and filter them by transfers.

    for transfer in transfers:
        if transfer["to"] != ACCOUNT_TO_MONITOR:
            continue

        try:
            post = Post(transfer.get("memo"))
        except ValueError as e:
            if 'Invalid identifier' == e.args[0]:
                print("Invalid post link. Consider a refund. [%s]" %
                      transfer.get("memo"))
                continue
        # Do something here if we can get the memo successfuly.
        # https://gist.githubusercontent.com/emre/6bffc3c08c99d5060a69f6b16e2051ea/raw/d0c1fbe81ebb4692f6010122f2193a8ffc777b9c/get_account_history_output.txt
        memo = transfer.get("memo")
        transferfrom = transfer.get("from")
        transaction_id = transfer.get("trx_id")
        transaction_timestamp = transfer.get("timestamp")
        transaction_amount = transfer.get("amount")
        #transaction_currency = 

        print("Memo : ", memo)
        print("Transfer from : ",transferfrom)
        print("Transaction ID : ", transaction_id)
        print("Transaction Timestamp : ",transaction_timestamp)
        print("Transaction Amount : ",transaction_amount )

     

        logger.load_transactionlog_table(transaction_id,transferfrom,transaction_timestamp,transaction_amount,memo,conn)

        # get a voting weight between 1 and 100
        # vote_weight = float(random.randint(+1, +100))
        # post.commit.vote(post.identifier, vote_weight, account=BOT_ACCOUNT)

        print("Sleeping for 3 seconds")
        time.sleep(3)

