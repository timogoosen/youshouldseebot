#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3


class SQLiteLogger(object):

    # Stuff Related to Logging dirsearch to sqlite

    def setup_tables(self, conn):
        cur = conn.cursor()

# Using the URL for the primary key as you can never have two posts with the same URL.

        cur.executescript('''

        CREATE TABLE IF NOT EXISTS EarningLog (
            url	TEXT primary key,
            created_date TEXT,
            title text,
            username	TEXT,
            total_post_reward	REAL,
            curation_share REAL
        );

        ''')


        # When we work with datetime we can use this:
        # https://www.sqlite.org/lang_datefunc.html
        # sqlite> select datetime("2018-07-31 22:43:15");
        # 2018-07-31 22:43:15



    def connection(self, db):
        conn = sqlite3.connect(db)
        return conn

    def close_conection(self, conn):
        conn.close()


    def load_earninglog_table(self, post_url,post_created_date,post_title,username,total_post_reward,curation_share, conn):
        cur = conn.cursor()

        cur.execute(
            '''select exists(select url from EarningLog where url=?)''', ( post_url, ) )
        exists = cur.fetchone()[0]

        if exists:
            print ("Post with that URL already exists")


        else:
            print ("Post with that URL does not exist lets insert url into db")
            cur.execute('''INSERT OR IGNORE INTO EarningLog (url,created_date,title,username,total_post_reward,curation_share)
        VALUES ( ?,?,?,?,?,?)''', ( post_url,post_created_date,post_title,username,total_post_reward,curation_share ) )

            # Catch error here

            # Commit Changes
            conn.commit()
    # We need some methods to validate data especially the content size

